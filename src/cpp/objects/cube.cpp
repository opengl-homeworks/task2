#include "cube.h"

#include <vector>

// The cube radius
const float R = 1;

// The length of the half of the cube edge
const float A = R / std::sqrt(3);

// The coordinates of the cube vertices
static float positions[] {
    -A, A, -A,
    A, A, -A,
    -A, -A, -A,
    A, -A, -A,
    -A, A, A,
    A, A, A,
    -A, -A, A,
    A, -A, A
};

// The normals of the cube faces
static float normals[] {
    -1, 0, 0,
    1, 0, 0,
    0, -1, 0,
    0, 1, 0,
    0, 0, -1,
    0, 0, 1
};

// The indices of the triangles
static int indices[] {
    0, 2, 4,
    2, 6, 4,
    5, 7, 1,
    7, 3, 1,
    6, 2, 7,
    2, 3, 7,
    0, 4, 1,
    4, 5, 1,
    1, 3, 0,
    3, 2, 0,
    4, 6, 5,
    6, 7, 5
};

Cube::Cube(glm::vec3 color) : color(color) { }

Cube::Cube(glm::vec3 color, glm::vec3 position) : color(color), Object3D(position) { }

void Cube::initialize() {
    std::vector<GLfloat> vertices;

    for (int i = 0; i < 36; i++) {
        vertices.push_back(positions[indices[i] * 3]);
        vertices.push_back(positions[indices[i] * 3 + 1]);
        vertices.push_back(positions[indices[i] * 3 + 2]);

        vertices.push_back(normals[i / 6 * 3]);
        vertices.push_back(normals[i / 6 * 3 + 1]);
        vertices.push_back(normals[i / 6 * 3 + 2]);
    }

    // The vertex array object
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // The vertex buffer object
    GLuint vbo;
    glGenBuffers(1, &vbo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

    // The a_position attribute
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*) 0);

    // The a_normal attribute
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*) (3 * sizeof(GLfloat)));
}

void Cube::draw() const {
    glBindVertexArray(vao);

    glDrawArrays(GL_TRIANGLES, 0, 36);
}
