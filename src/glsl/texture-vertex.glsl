#version 400 core

layout (location = 0) in vec3 a_position; // The vertex coordinates
layout (location = 1) in vec2 a_textureCoordinates; // The texture coordinates

uniform mat4 u_matrix; // The transformation matrix

out vec2 v_textureCoordinates; // The texture coordinates that are passed to the fragment shader and interpolated

void main() {
    v_textureCoordinates = a_textureCoordinates; // Pass the texture coordinates
    gl_Position = u_matrix * vec4(a_position, 1.0); // Calculate the vertex position in the clip space
}
