#include "planet.h"

#include <glm/ext.hpp>

#include <vector>

#include "../loaders/image/load_image.h"

const float PI = glm::pi<float>();

// The planet radius
const float R = 5;

// The number of vertical segments
const int M = 20;

// The number of horizontal segments
const int N = 40;

// The rotation speed of the planet
const float ROTATION_SPEED = 6;

Planet::Planet(std::string imagePath) : imagePath(std::move(imagePath)) { }

Planet::Planet(std::string imagePath, glm::vec3 position) : imagePath(std::move(imagePath)), Object3D(position) { }

void Planet::initialize() {
    std::vector<GLfloat> vertices;

    for (int i = 0; i < N; i++) {
        vertices.push_back(0);
        vertices.push_back(R);
        vertices.push_back(0);

        vertices.push_back(1 / (float) N / 2 + (float) i / (float) N);
        vertices.push_back(1);
    }

    for (int i = 1; i < M; i++) {
        float a = PI * (float) i / (float) M;

        float h = R * std::cos(a);
        float l = R * std::sin(a);

        for (int j = 0; j <= N; j++) {
            float b = 2 * PI * (float) j / (float) N;

            vertices.push_back(l * std::sin(b));
            vertices.push_back(h);
            vertices.push_back(l * std::cos(b));

            vertices.push_back((float) j / (float) N);
            vertices.push_back(1 - (float) i / (float) M);
        }
    }

    for (int i = 0; i < N; i++) {
        vertices.push_back(0);
        vertices.push_back(-R);
        vertices.push_back(0);

        vertices.push_back((float) i / (float) N);
        vertices.push_back(0);
    }

    std::vector<GLuint> indices;

    for (int i = N; i < N * 2; i++) {
        indices.push_back(i);
        indices.push_back(i + 1);
        indices.push_back(i - N);
    }

    for (int i = 1; i < M - 1; i++) {
        for (int j = 0; j < N; j++) {
            int index = N + (N + 1) * i + j;

            indices.push_back(index);
            indices.push_back(index + 1);
            indices.push_back(index - (N + 1));

            indices.push_back(index + 1);
            indices.push_back(index + 1 - (N + 1));
            indices.push_back(index - (N + 1));
        }
    }

    for (int i = N + (N + 1) * (M - 2); i < N + (N + 1) * (M - 2) + N; i++) {
        indices.push_back(i);
        indices.push_back(i + (N + 1));
        indices.push_back(i + 1);
    }

    // The vertex array object
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // The vertex buffer object
    GLuint vbo;
    glGenBuffers(1, &vbo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

    // The a_position attribute
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*) 0);

    // The a_textureCoordinates attribute
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (void*) (3 * sizeof(GLfloat)));

    // The index buffer object
    GLuint ibo;
    glGenBuffers(1, &ibo);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

    indicesCount = indices.size();

    // Load the planet texture image (the texture is expected to be stored upside down so the flip is required)
    Image image = loadImage(imagePath, true);

    // The texture
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.bytes.data());
}

void Planet::update(float dt) {
    // Rotate the planet around the y axis
    rotation.y += glm::radians(ROTATION_SPEED) * dt;

    // Limit the planet rotation to 360 degrees
    rotation.y = std::fmod(rotation.y, PI * 2);
}

void Planet::draw() const {
    glBindVertexArray(vao);

    glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, (void*) 0);
}
