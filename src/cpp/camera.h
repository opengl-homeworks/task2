#pragma once

#include <glm/glm.hpp>
#include <glm/ext.hpp>

/**
 * The camera
 */
class Camera {

public:

    /**
     * The camera position
     */
    glm::vec3 position = glm::vec3(0, 0, 0);

    /**
     * The camera rotation angle in the horizontal plane
     */
    float horizontal = 0;

    /**
     * The camera rotation angle in the vertical plane
     */
    float vertical = 0;

public:

    /**
     * Constructor
     */
    Camera();

    /**
     * Constructor
     * @param x the camera x coordinate
     * @param y the camera y coordinate
     * @param z the camera z coordinate
     */
    Camera(float x, float y, float z);

    /**
     * Rotates the camera in the horizontal plane
     * @param angle the rotation angle
     */
    void rotateHorizontally(float angle);

    /**
     * Rotates the camera in the vertical plane
     * @param angle the rotation angle
     */
    void rotateVertically(float angle);

    /**
     * Returns the forward vector for the camera (where the camera is pointed)
     * @return the forward vector for the camera
     */
    glm::vec3 getForward() const;

    /**
     * Returns the right vector for the camera (orthogonal to the forward vector)
     * @return the right vector for the camera
     */
    glm::vec3 getRight() const;

    /**
     * Returns the up vector for the camera (orthogonal to the forward and right vectors)
     * @return the up vector for the camera
     */
    glm::vec3 getUp() const;

};

