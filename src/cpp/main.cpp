#define GLFW_INCLUDE_NONE

#include <GLFW/glfw3.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <iostream>

#include "loaders/text/load_text.h"
#include "loaders/image/load_image.h"
#include "utils/opengl.h"
#include "camera.h"
#include "objects/planet.h"
#include "objects/cube.h"

const float PI = glm::pi<float>();

// The field of view
const float FOV_Y = 70;

// The camera rotation speed
const float CAMERA_ROTATION_SPEED = 60;

// The camera movement speed
const float CAMERA_MOVEMENT_SPEED = 6;

// The paths to the skybox texture images
std::string skyboxPaths[] {
    "../res/image/skybox/right.png",
    "../res/image/skybox/left.png",
    "../res/image/skybox/up.png",
    "../res/image/skybox/down.png",
    "../res/image/skybox/front.png",
    "../res/image/skybox/back.png"
};

// The coordinates of the viewport corners
GLfloat skyboxVertices[] {
    -1, 1,
    -1, -1,
    1, 1,
    1, -1
};

// The GLFW window
GLFWwindow* window;

// The viewport width
int width = 640;

// The viewport height
int height = 640;

// The viewport aspect
float aspect = (float) width / (float) height;

// The mouse x coordinate
double mouseX = 0;

// The mouse y coordinate
double mouseY = 0;

// Shows whether the mouse is captured
bool mouseCaptured;

// The elapsed time since the GLFW initialization in seconds
float t;

// The elapsed time since the last frame in seconds
float dt;

// The OpenGL variables for the skybox program
GLuint skyboxProgram;
struct {
    GLuint u_forward;
    GLuint u_clipPlaneRight;
    GLuint u_clipPlaneUp;
    GLuint u_texture;
} skyboxUniforms;
GLuint skyboxVao;
GLuint skyboxTexture;

// The OpenGL variables for the color program
GLuint colorProgram;
struct  {
    GLuint u_forward;
    GLuint u_color;
    GLuint u_normalMatrix;
    GLuint u_matrix;
} colorUniforms;

// The OpenGL variables for the texture program
GLuint textureProgram;
struct {
    GLuint u_matrix;
    GLuint u_texture;
} textureUniforms;

// The camera
Camera camera(0, 0, 20);

// The camera vectors
glm::vec3 forward;
glm::vec3 right;
glm::vec3 up;

// The projection-view matrix
glm::mat4 projectionViewMatrix;

// The planet
Planet planet("../res/image/mars.png");

// The cube
Cube cube(glm::vec3(1, 0, 0), glm::vec3(10, 0, 0));

// Initializes the 3D scene in OpenGL
void initialize();

// Initializes the skybox program
void initializeSkyboxProgram();

// Initializes the color program
void initializeColorProgram();

// Initializes the texture program
void initializeTextureProgram();

// Initializes the skybox in OpenGL
void initializeSkybox();

// Resizes the viewport
void resize();

// Updates the 3D scene
void update();

// Updates the camera
void updateCamera();

// Updates the projection-view matrix
void updateProjectionViewMatrix();

// Draws the 3D scene
void draw();

// Draws the skybox
void drawSkybox();

void glfwErrorCallback(int error, const char* description) {
    std::cerr << "The GLFW function call completed with errors:\n" << description << std::endl;
}

void glfwCursorPosCallback(GLFWwindow* window, double xpos, double ypos) {
    if (mouseCaptured) {
        camera.rotateHorizontally((xpos - mouseX) * glm::radians(0.1f));
        camera.rotateVertically(-(ypos - mouseY) * glm::radians(0.1f));

        mouseX = xpos;
        mouseY = ypos;
    }
}

void glfwMouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        mouseCaptured = true;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        glfwGetCursorPos(window, &mouseX, &mouseY);
    }
}

void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        mouseCaptured = false;
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
}

int main() {
    glfwSetErrorCallback(glfwErrorCallback);

    if (!glfwInit()) return -1;

    // Set the OpenGL version window hints
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    // Set the OpenGL context window hints (required for macOS)
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "Task 2", nullptr, nullptr);

    // Close the application if the window isn't created
    if (!window) {
        glfwTerminate();
        return -1;
    }

    // Set the callbacks for controls
    glfwSetCursorPosCallback(window, glfwCursorPosCallback);
    glfwSetMouseButtonCallback(window, glfwMouseButtonCallback);
    glfwSetKeyCallback(window, glfwKeyCallback);

    // Enable the raw mouse motion for the camera control
    if (glfwRawMouseMotionSupported()) glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    else std::cout << "Raw mouse motion isn't supported" << std::endl;

    glfwMakeContextCurrent(window);

    // Limit the frame rate to a screen refresh rate
    glfwSwapInterval(1);

    GLenum glewStatus = glewInit();

    if (glewStatus != GLEW_OK) {
        std::cerr << "The GLEW initialization completed with errors:\n" << glewGetErrorString(glewStatus) << std::endl;
        glfwTerminate();
        return -1;
    }

    try {
        initialize();
    }
    catch (const std::exception& e) {
        // Close the application if there is an error while loading the files, compiling the shaders or linking the program
        std::cerr << e.what() << std::endl;
        glfwTerminate();
        return -1;
    }

    // Enable GL_CULL_FACE to draw only the visible faces
    glEnable(GL_CULL_FACE);

    // Get the initial time
    t = glfwGetTime();

    while (!glfwWindowShouldClose(window)) {
        // Don't update and draw the scene if the program window is minimized
        if (glfwGetWindowAttrib(window, GLFW_ICONIFIED)) {
            glfwWaitEvents();
            continue;
        }

        // Calculate the elapsed time
        float nt = glfwGetTime();
        dt = nt - t;
        t = nt;

        resize();
        update();
        draw();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

void initialize() {
    initializeSkyboxProgram();
    initializeColorProgram();
    initializeTextureProgram();

    initializeSkybox();

    planet.initialize();

    cube.initialize();
}

void initializeSkyboxProgram() {
    std::string vertexShaderSource = loadText("../src/glsl/skybox-vertex.glsl");
    std::string fragmentShaderSource = loadText("../src/glsl/skybox-fragment.glsl");

    GLuint vertexShader = createShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);

    skyboxProgram = createProgram(vertexShader, fragmentShader);

    skyboxUniforms.u_forward = glGetUniformLocation(skyboxProgram, "u_forward");
    skyboxUniforms.u_clipPlaneRight = glGetUniformLocation(skyboxProgram, "u_clipPlaneRight");
    skyboxUniforms.u_clipPlaneUp = glGetUniformLocation(skyboxProgram, "u_clipPlaneUp");
    skyboxUniforms.u_texture = glGetUniformLocation(skyboxProgram, "u_texture");

    glUseProgram(skyboxProgram);

    glUniform1f(skyboxUniforms.u_texture, 0);
}

void initializeColorProgram() {
    std::string vertexShaderSource = loadText("../src/glsl/color-vertex.glsl");
    std::string fragmentShaderSource = loadText("../src/glsl/color-fragment.glsl");

    GLuint vertexShader = createShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);

    colorProgram = createProgram(vertexShader, fragmentShader);

    colorUniforms.u_forward = glGetUniformLocation(colorProgram, "u_forward");
    colorUniforms.u_color = glGetUniformLocation(colorProgram, "u_color");
    colorUniforms.u_normalMatrix = glGetUniformLocation(colorProgram, "u_normalMatrix");
    colorUniforms.u_matrix = glGetUniformLocation(colorProgram, "u_matrix");
}

void initializeTextureProgram() {
    std::string vertexShaderSource = loadText("../src/glsl/texture-vertex.glsl");
    std::string fragmentShaderSource = loadText("../src/glsl/texture-fragment.glsl");

    GLuint vertexShader = createShader(vertexShaderSource, GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader(fragmentShaderSource, GL_FRAGMENT_SHADER);

    textureProgram = createProgram(vertexShader, fragmentShader);

    textureUniforms.u_matrix = glGetUniformLocation(textureProgram, "u_matrix");
    textureUniforms.u_texture = glGetUniformLocation(textureProgram, "u_texture");

    glUseProgram(textureProgram);

    glUniform1f(textureUniforms.u_texture, 0);
}

void initializeSkybox() {
    // Create and bind the vertex array object for the skybox
    glGenVertexArrays(1, &skyboxVao);
    glBindVertexArray(skyboxVao);

    // Create, bind and fill the vertex buffer object for the skybox
    GLuint skyboxVbo;
    glGenBuffers(1, &skyboxVbo);
    glBindBuffer(GL_ARRAY_BUFFER, skyboxVbo);
    glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), &skyboxVertices, GL_STATIC_DRAW);

    // The a_position attribute
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);

    // Load the skybox texture images (the cubemap texture is expected to be stored as usual so no flip is required)
    Image skyboxImages[6];

    for (int i = 0; i < 6; i++) skyboxImages[i] = loadImage(skyboxPaths[i], false);

    // Create the cubemap texture
    glGenTextures(1, &skyboxTexture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    // Fill the cubemap texture with the data from the skybox texture images
    for (int i = 0; i < 6; i++) {
        Image& image = skyboxImages[i];
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.bytes.data());
    }
}

void resize() {
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);
    aspect = (float) width / (float) height;
}

void update() {
    updateCamera();
    updateProjectionViewMatrix();

    planet.update(dt);
}

void updateCamera() {
    if (!mouseCaptured) {
        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) camera.rotateHorizontally(-glm::radians(CAMERA_ROTATION_SPEED) * dt);
        else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) camera.rotateHorizontally(glm::radians(CAMERA_ROTATION_SPEED) * dt);

        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) camera.rotateVertically(-glm::radians(CAMERA_ROTATION_SPEED) * dt);
        else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) camera.rotateVertically(glm::radians(CAMERA_ROTATION_SPEED) * dt);
    }

    forward = camera.getForward();
    right = camera.getRight();
    up = camera.getUp();

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) camera.position -= right * CAMERA_MOVEMENT_SPEED * dt;
    else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) camera.position += right * CAMERA_MOVEMENT_SPEED * dt;

    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) camera.position -= forward * CAMERA_MOVEMENT_SPEED * dt;
    else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) camera.position += forward * CAMERA_MOVEMENT_SPEED * dt;

    if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS) camera.position.y -= CAMERA_MOVEMENT_SPEED * dt;
    else if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) camera.position.y += CAMERA_MOVEMENT_SPEED * dt;
}

void updateProjectionViewMatrix() {
    glm::mat4 projectionMatrix = glm::perspective(glm::radians(FOV_Y), aspect, 0.1f, 100.0f);
    glm::vec3 center = camera.position + forward;
    glm::mat4 viewMatrix = glm::lookAt(camera.position, center, up);
    projectionViewMatrix = projectionMatrix * viewMatrix;
}

void draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    drawSkybox();

    // Enable GL_DEPTH_TEST to draw the objects
    glEnable(GL_DEPTH_TEST);

    //The normal matrix
    glm::mat4 normalMatrix;

    // The transformation matrix
    glm::mat4 matrix;

    // Use the color program to draw colored objects
    glUseProgram(colorProgram);

    normalMatrix = cube.getNormalMatrix();

    // Calculate the transformation matrix for the cube
    matrix = projectionViewMatrix * cube.getModelMatrix();

    // Set the program uniforms
    glUniform3fv(colorUniforms.u_forward, 1, (GLfloat*) &forward);
    glUniform3fv(colorUniforms.u_color, 1, (GLfloat*) &cube.color);
    glUniformMatrix4fv(colorUniforms.u_normalMatrix, 1, GL_FALSE, (GLfloat*) &normalMatrix);
    glUniformMatrix4fv(colorUniforms.u_matrix, 1, GL_FALSE, (GLfloat*) &matrix);

    // Draw the cube
    cube.draw();

    // Use the texture program to draw textured objects
    glUseProgram(textureProgram);

    // Bind the planet texture to the texture slot 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, planet.texture);

    // Calculate the transformation matrix for the planet
    matrix = projectionViewMatrix * planet.getModelMatrix();

    // Set the program uniforms
    glUniformMatrix4fv(textureUniforms.u_matrix, 1, GL_FALSE, (GLfloat*) &matrix);

    // Draw the planet
    planet.draw();

    // Disable GL_DEPTH_TEST to draw the skybox
    glDisable(GL_DEPTH_TEST);
}

void drawSkybox() {
    // Use the skybox program to draw the skybox
    glUseProgram(skyboxProgram);

    // Bind the vertex array object for the skybox
    glBindVertexArray(skyboxVao);

    // Bind the skybox texture to the texture slot 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexture);

    // Set the uniform for the forward vector
    glUniform3fv(skyboxUniforms.u_forward, 1, (GLfloat*) &forward);

    // Calculate the clip plane vectors
    float tanY = std::tan(glm::radians(FOV_Y / 2));
    float tanX = tanY * aspect;

    glm::vec3 clipPlaneRight = tanX * right;
    glm::vec3 clipPlaneUp = tanY * up;

    // Set the uniforms for the clip plane vectors
    glUniform3fv(skyboxUniforms.u_clipPlaneRight, 1, (GLfloat*) &clipPlaneRight);
    glUniform3fv(skyboxUniforms.u_clipPlaneUp, 1, (GLfloat*) &clipPlaneUp);

    // Draw the skybox
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
