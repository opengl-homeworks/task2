#include "camera.h"

const float PI = glm::pi<float>();

Camera::Camera() = default;

Camera::Camera(float x, float y, float z): position(x, y, z) { }

void Camera::rotateHorizontally(float angle) {
    horizontal += angle;

    horizontal = std::fmod(horizontal, PI * 2);
}

void Camera::rotateVertically(float angle) {
    vertical += angle;

    if (vertical > PI / 2) vertical = PI / 2;
    else if (vertical < -PI / 2) vertical = -PI / 2;
}

glm::vec3 Camera::getForward() const {
    float projection = std::cos(vertical);

    return glm::vec3(projection * std::sin(horizontal), std::sin(vertical), -projection * std::cos(horizontal));
}

glm::vec3 Camera::getRight() const {
    return glm::vec3(std::sin(horizontal + PI / 2), 0, -std::cos(horizontal + PI / 2));
}

glm::vec3 Camera::getUp() const {
    float projection = std::cos(vertical + PI / 2);

    return glm::vec3(projection * std::sin(horizontal), std::sin(vertical + PI / 2), -projection * std::cos(horizontal));
}
