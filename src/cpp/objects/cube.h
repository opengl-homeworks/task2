#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "object3d.h"

/**
 * The colored cube
 */
class Cube : public Object3D {

public:

    /**
     * The cube color
     */
    glm::vec3 color;

public:

    /**
     * Constructor
     * @param color the cube color
     */
    explicit Cube(glm::vec3 color);

    /**
     * Constructor
     * @param color the cube color
     * @param position the cube position
     */
    Cube(glm::vec3 color, glm::vec3 position);

    void initialize() override;

    void draw() const override;

};
